#+LATEX_CLASS: article
#+TITLE: Geometry Lost #2
#+AUTHOR: Joakim Verona, Per Weijnitz, AI, SGC
#+DATE: Present day
#+OPTIONS: author:t date:t ^:nil toc:nil
#+ATTR_LATEX: :width 5cm

# Load packages needed for background shapes
#+LaTeX_HEADER: \usepackage{background}
#+LaTeX_HEADER: \usetikzlibrary{calc}
#+LaTeX_HEADER: \usepackage{hyperref}

# Customize main background and text colors
#+LaTeX_HEADER: \pagecolor[rgb]{0.35,0.35,0.35}   %gray
#+LaTeX_HEADER: \color[rgb]{1,1,1}       %white

# Format the title page:
#+LaTeX_HEADER: \usepackage{titling}
#+LaTeX_HEADER: \pretitle{\begin{flushleft}\begin{Large}}
#+LaTeX_HEADER: \posttitle{\end{Large}\end{flushleft}}
#+LaTeX_HEADER: \preauthor{\begin{flushleft}\begin{Large}}
#+LaTeX_HEADER: \postauthor{\end{Large}\end{flushleft}}
#+LaTeX_HEADER: \predate{\begin{flushleft}}
#+LaTeX_HEADER: \postdate{\end{flushleft}}
#+LaTeX_HEADER: \setcounter{secnumdepth}{0}

#+BEGIN_EXPORT latex
\newcommand{\SetBgImage}[3]{
  \backgroundsetup{
    scale=1,
    angle=0,
    opacity=1,
    color=white,
    contents={
      \begin{tikzpicture}[overlay,remember picture]
        \draw[line width=5pt,rounded corners=10pt]
          ($(current page.north west) + (1cm,-1cm)$)
          rectangle
          ($(current page.south east) + (-1cm,1cm)$);
        \node[anchor=north east] at ($(current page.north east) + (-1.5cm,-1.5cm)$) {\includegraphics[width=2.5cm]{#1}};
        \node[anchor=south east] at ($(current page.south east) + (-1.5cm,1.5cm)$) {\includegraphics[width=2.5cm]{#2}};
        \node[anchor=south west] at ($(current page.south west) + (1.5cm,1.5cm)$) {\includegraphics[width=2.5cm]{#3}};
      \end{tikzpicture}
    }
  }
}
\AtEndDocument{\SetBgImage{}{}{}}
#+END_EXPORT

\pagenumbering{gobble}


# TODO: it technically works to supply three empty file names as shape images,
# and can be used to produce pages without shapes, but it produces ugly
# latex compilation errors, so see if we can find a way to add conditionals to the
# SetBgImage image insertion parts.
\SetBgImage{}{}{}

# What are all those $SHAPE0 variables below you ask.
# They are envsubsted from the Makefile

\newpage
\SetBgImage$SHAPE0
\pagenumbering{arabic}
\setcounter{page}{1}
#+INCLUDE: articles/sthlm-geometry.org

\newpage
\SetBgImage{}{}{}
\setcounter{savepage}{\value{page}}
\pagenumbering{gobble}
file:assets/geometry-lost.png

\newpage
\SetBgImage$SHAPE1
\pagenumbering{arabic}
\setcounter{page}{3}
#+INCLUDE: articles/geometric-man.org

\newpage
\SetBgImage$SHAPE2
#+INCLUDE: articles/geometric-metafors.org

\newpage
\SetBgImage$SHAPE3
#+INCLUDE:  articles/geometrical-dreams.org

\newpage
\SetBgImage{}{}{}
\pagenumbering{gobble}
file:assets/geometric-man.png
file:assets/geometry-failed.png

\newpage
\SetBgImage$SHAPE4
\pagenumbering{arabic}
\setcounter{page}{11}
#+INCLUDE:  articles/boullee.org
