* Geometry, Sublimity, and the Modern Urban Landscape

\vspace{0.7cm}

\textit{Reviving Boullée's Vision in a Postmodern World}

\vspace{1.0cm}

Postmodern society. Urban landscapes dominated by
endless horizons of uninspired concrete boxes. Absence of
architectural poetry. Citizens lost and disconnected.

\vspace{1.0cm}

Étienne-Louis Boullée was an 18th-century architect who dared to
envision a different world — one where geometric shapes were wielded
like emotional instruments, sculpting tailored experiences for
beholders. Spheres, cones and pyramids were carefully composed into
emoting monuments, evoking the feelings that were appropriate
for the function of the building. A cenotaph would inspire a different
emotion than a grand hotel. He would utilize vastness as an amplifier
of impression, drawing from ancient Greek notions of the sublime.

Ideas once praised are now lost and replaced with others.

[[file:../assets/Boullée_-_Cénotaphe_égyptien_-_élévation.jpg]]

\newpage

\textit{Boullée's Vision and the Emotive Power of Geometry}

\vspace{1.0cm}

For Boullée, architecture was not merely a utilitarian endeavor but a
profound means of shaping human experience. Central to his philosophy
was the belief that specific geometric forms could elicit nuanced and
tailored emotional responses in beholders. In a time when architecture
was often bound by classical principles, Boullée's radical ideas paved
the way for a new understanding of the built environment as a conduit
for the expression of emotion and transcendence.

According to Boullée, spheres, with their sense of universality and
infinity, were capable of instilling awe and reverence. Cylinders,
symbolizing stability and harmony, could evoke feelings of serenity
and equilibrium. Pyramids and cones, with their upward thrust,
inspired aspirations and a sense of transcendence.

Boullée's ideas were not mere theoretical conjectures; they were
manifest in his architectural designs. His masterpiece, the "Cenotaph
for Newton" stands as a testament to his vision for emotional
architecture. In this monumental structure conceived as a tribute to
the eminent scientist Isaac Newton, Boullée employed a colossal sphere
— a symbol of the cosmos — to envelop visitors in a transcendent
experience. The cavernous interior, illuminated by a single source of
light, heightened the sense of awe and contemplation, inviting
visitors to ponder the mysteries of the universe.

[[file:../assets/Newton_memorial_boullee.jpg]]

His projects often transcended mere functionality to
provoke profound psychological and spiritual responses. The Project
for a Metropolitan Church envisioned a monumental dome that soared
heavenward, evoking a sense of spiritual elevation. The Design for a
Royal Library proposed a cylindrical form adorned with celestial
motifs, invoking the grandeur of knowledge and enlightenment.

[[file:../assets/Etienne-Louis_Boullée,_Perspective_View_of_the_Interior_of_a_Metropolitan_Church_small.jpg]]

In Boullée's architectural visions, geometry ceased to be a tool of
construction; it became a language of emotion and meaning. His designs
challenged viewers to transcend the mundane and engage with
architecture as a transformative force.



\newpage

\textit{Failing the Greek: Vastness without the Sublime}

\vspace{1.0cm}

The concept of the sublime, emerging from ancient Greek philosophy via
Longinus's works, embodies the awe evoked by vast phenomena, be they
artistic or natural, such as towering mountains and expansive
oceans. Boullée understood this, and used it to enhance the
impressions of his creations.

In the modern urban landscape, skyscrapers dominate the skyline,
boasting impressive heights and engineering feats. However, despite
their towering size, many modern skyscrapers fail to evoke the same
sense of sublimity as natural wonders or historically significant
architecture. This paradox arises from the disconnect between sheer
scale and emotional resonance. While skyscrapers may impress with
their enormity, they often lack the organic beauty and narrative depth
found in natural landscapes or culturally rich architectural
masterpieces.

Despite their monumental presence, modern skyscrapers are often
characterized by uniformity, repetition, and a focus on efficiency
rather than aesthetic or emotional impact. The result is a cityscape
punctuated by sterile glass and steel monoliths, devoid of the
awe-inspiring qualities that define the sublime. In contrast to the
sublime's association with the vast and untamed forces of nature,
these structures symbolize human ambition and technological prowess
but may fall short in stirring the depths of human emotion and
imagination.

As we navigate the urban jungle of modernity, we are confronted with
the paradox of skyscrapers that loom large yet leave us longing for
the sublime experiences afforded by nature and the great works of
architectural history. In this tension between scale and significance,
we are compelled to question the true essence of the sublime and how
it can be realized in an increasingly urbanized world.


\newpage

\textit{The Mathematics of Emotion}

\vspace{1.0cm}

Since the times of Longinus and Boullée, numerous studies have shed
light on the profound effects of architectural design on mental
well-being and emotional experiences in urban environments. Research
has shown that factors such as natural light, spatial layout, and
material texture can significantly influence mood and cognition. For
example, environments characterized by open, airy spaces and
curvilinear forms tend to promote feelings of relaxation and
creativity, whereas environments dominated by rigid, angular
structures may induce stress and agitation. By synthesizing findings
from Longinus, Boullée, environmental psychology, sociology, urban
planning, with computer science and AI, there is no reason why
scholars could not construct a computer system that would output
architectural designs according to utilitarian specifications, that
would be embodied in geometric shapes composed to maximise the mental
well being of humans.

In a world increasingly defined by cold, utilitarian structures,
Boullée's radical vision of emotional architecture offers a compelling
alternative. By reimagining the role of geometry in urban design and
drawing inspiration from ancient concepts of the sublime, we can
strive to create cityscapes that inspire awe, foster connection, and
elevate the human spirit.

As we contemplate the future of our built environment, let us ponder:
can we resurrect Boullée's dream of architecture as a medium for
emotional transcendence, or are we doomed to inhabit a world of
soulless rectangles, forever devoid of meaning?  In which direction
would we like to strive for the future, and what does the design of
our artifacts say about humanity?

\vspace{1.0cm}

Images from Wikipedia and Wikimedia.
